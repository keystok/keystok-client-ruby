# Keystok

## Tests status
![Build status](https://www.codeship.io/projects/65218fa0-9e09-0131-3f96-16b8a84518ac/status)

## Installation

    $ gem install keystok

## Usage

    require 'keystok'
    require 'yaml' # Config is then easier

    keystok = Keystok::Client.new('das21312312_access_token')
    # or config can be loaded from file:
    # config = YAML.load_file('keystok.yml')
    # keystok = Keystok::Client.new(config)

    # get hash with all keys
    keystok.keys

    # get one key
    keystok.get(key_id)

### Rails integration

Add keystok to Gemfile:

    gem 'keystok'

Keystok have generator that can create config and initializer for you

    rails g keystok:install access_token_value

In Rails env Keystok will use Rails.logger. Without Rails it will default to logging to STDOUT.
If you would like to define your own logger you can do it like:

    Keystok.logger = your_logger

## Config options description

### **api_host**, **auth_host**

Those values are not needed by default. It can be required if you are using separated Keystok servers

### **access_token**

This value is visible in **API Access** app tab in Keystok.com

### **eager_fetching**

When **true**, client will fetch all keys from API even when one was requested. This will save time
when another key will be requested, because client will not have to fetch it from API.
However in some cases like huge amount of keys or when **volatile** is set,
this option should be set to **false**. Default value is **true**

### **no_cache**

If you don't want to use cache functionality, you can disable cache writing by setting **no_cache** to **true** in config

### **tmp_dir**

This dir will be used to store encrypted cache which can be used when API can't be contacted

### **volatile**

Normally when asking about key that is in local store, SDK will not perform API request to provide quicker response.
Request can be forced on per method call basis by second parameter set to **true** (default is **false**)

    keystok.get('this_can_change_often', true)

However if you want SDK to make request on every request without adding **true** to every **get** or **keys**,
you can set **volatile** to **true** in config. This will force SDK to ask API on every request.
Please note that this will may decrese performance of you app since it will require API request
and cache writing on every **get** and **keys** call

## CLI client (since v1.1.0)

CLI client purpose is to be able to easily use Keystok in for example shell scripts when you already have Keystok Ruby SDK installed.
### Example usage
Get key content:

    $ keystok_rb -k my_key

Get keys list:

    $ keystok_rb

Get all keys with content in CSV format:

    $ keystok_rb -a dump

By default **keystok_rb** search for file with access token in **~/.keystok.yml**. File format is just:

    ---
    :access_token: access_token_value

It's named keystok_rb to not interfere with other SDK's and you are free to create symlink or RVM wrapper with any name you want.

### Options

#### **-a** **--action**

Possible values:
* dump - get all keys with content and print output
* get - print key content or list keys if **-k** is not used (default)

#### **-c** **--access-token**

Specify access token

#### **-f** **--access-token-file**

Specify access token filepath (default is **~/.keystok.yml**)
Example file format:

    ---
    :access_token: access_token_value

NOTE: **-c** have higher precedence than **-f**

#### **-i** **--init-token-file**

When used with **-c** it creates token config file. It's an easy way to create config file while executing first command.
Please note that **-f** have effect on this.

#### **-k** **--key-id**

Specify which key should be fetched when action is **get**

#### **-t** **--dump-format-type**

Possible values:
* csv (default)
* json
* yaml

Prints all keys with their content in specified format

#### **-h** **--help**

Print help and exit
