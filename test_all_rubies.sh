#!/bin/bash -e

versions=("1.9.3" "2.0.0-p353" "2.0.0-p451" "2.1.0" "2.1.1" "ruby-head")
echo "================"
echo "Installing dependencies:"
echo "================"
for version in "${versions[@]}"
do
echo "================"
  echo "$version:"
  rvm $version exec bundle install
done
echo "================"
echo "Running tests:"
echo "================"
for version in "${versions[@]}"
do
echo "================"
  echo "$version:"
  rvm $version exec bundle exec rspec spec
done
