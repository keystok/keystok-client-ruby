API_HOST = 'https://api.keystok.com'
DECRYPTION_KEY =
  '965391f2bba37b4586431b8690d7044d5cdc73adf7dda539f8dd3a60cb3e9b12'
KEYSTOK_OAUTH_REFRESH_URL = 'https://keystok.com/oauth/token'
REFRESHED_TOKEN =
  '2cb4583cd8a399f51db15da57d0679706ae975d2aa9d3c71f5fdd0255dd6a028'

def example_response
  File.open(File.expand_path('../../fixtures/response_data_00.data',
                             __FILE__)).read
end

def oauth_refresh_url
  "#{config[:auth_host]}/oauth/token"
end

def override_config_key(key, value = nil)
  config_tmp = client.instance_variable_get('@config')
  config_tmp[key] = value
  client.instance_variable_set('@config', config_tmp)
end

def refresh_response
  {
    'access_token' => REFRESHED_TOKEN,
    'token_type' => 'bearer', 'expires_in' => 124
  }.to_json
end

def request_path(key = nil)
  if key
    key_path = "/#{key}"
  else
    key_path = ''
  end
  "apps/#{config[:id]}/deploy#{key_path}?access_token=#{REFRESHED_TOKEN}"
end

def request_url(host = nil, path = nil)
  host ||= config[:api_host]
  path ||= request_path
  "#{host}/#{path}"
end

def stub_deployment_req(host = nil, path = nil)
  stub_request(:get, request_url(host, path))
    .to_return(status: 200, body: example_response, headers: {})
end
