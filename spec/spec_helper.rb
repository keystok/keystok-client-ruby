require 'simplecov'
SimpleCov.profiles.define 'keystok-client-ruby' do
  add_filter 'bin/keystok_rb'
  add_filter 'lib/keystok.rb'
  add_filter 'spec'
end
SimpleCov.start 'keystok-client-ruby'

require 'webmock/rspec'
require 'ffaker'
require 'keystok'

Dir[File.expand_path('../support/**/*.rb', __FILE__)].each do |filepath|
  require filepath
end

RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
  config.run_all_when_everything_filtered = true
  config.filter_run :focus

  config.order = 'random'
end
