# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'
require 'fileutils'

describe Keystok::Client do
  let(:config) do
    { api_host: 'https://api.keystok-host.com',
      auth_host: 'https://keystok-host.com',
      rt: 'token',
      dk: DECRYPTION_KEY,
      id: 1,
      tmp_dir: tmp_dir }
  end
  let(:client) { Keystok::Client.new(config) }
  let(:tmp_dir) { File.expand_path('../../../tmp', __FILE__) }
  let(:tmp_filepath) { File.join(tmp_dir, 'keystok_cache.data') }

  before do
    Keystok.logger = Logger.new(nil)
    # config based url's
    stub_deployment_req(config[:api_host], request_path)
    stub_request(:post, oauth_refresh_url)
      .to_return(status: 200, body: refresh_response,
                 headers: { 'Content-Type' => 'application/json' })
    # Keystok URL's
    stub_deployment_req(API_HOST, request_path)
    stub_request(:post, KEYSTOK_OAUTH_REFRESH_URL)
      .to_return(status: 200, body: refresh_response,
                 headers: { 'Content-Type' => 'application/json' })
  end

  it 'allow initialization with empty config' do
    Keystok::Client.new
    Keystok::Client.new('')
    Keystok::Client.new(access_token: '', tmp_dir: 'tmp')
  end

  describe 'default for' do
    it "'eager_fetching' if true" do
      expect(client.instance_variable_get('@config')[:eager_fetching])
        .to be_true
    end
  end

  describe 'access_token' do
    it 'perform request to auth_host' do
      client.send(:access_token)
      a_request(:post, oauth_refresh_url).should have_been_made.once
    end

    it 'respond with new token' do
      token = client.send(:access_token)
      expect(token.class).to eq(OAuth2::AccessToken)
      expect(token.token.length).to be > 0
    end

    it 'cached the token' do
      token_1 = client.send(:access_token)
      token_2 = client.send(:access_token)
      expect(token_2).to equal(token_1)
    end

    it 'raise ConfigError when refresh token is nil' do
      override_config_key(:rt, nil)
      expect do
        client.send(:access_token)
      end.to raise_error(Keystok::Error::ConfigError, 'SDK not configured')
    end

    context 'connection fail' do
      context 'Faraday exception' do
        it 'tries REQUEST_TRY_COUNT times before giving-up' do
          stub_request(:post, oauth_refresh_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          begin
            client.send(:access_token)
          # rubocop:disable HandleExceptions
          rescue Faraday::Error::ConnectionFailed
          # rubocop:enable HandleExceptions
            # Nothing here becouse we just want to check requests count
          end
          a_request(:post, oauth_refresh_url).should have_been_made
            .times(Keystok::Client::REQUEST_TRY_COUNT)
        end

        it 'is re-raising Faraday exception after 3 attempts' do
          stub_request(:post, oauth_refresh_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          expect do
            client.send(:access_token)
          end.to raise_error(Faraday::Error::ConnectionFailed)
        end

        it 'should log warning on every Faraday exception' do
          stub_request(:post, oauth_refresh_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          logger = double(:logger)
          Keystok.logger = logger
          expect(logger).to receive(:warn).exactly(3).times
            .with('Exception during token refresh: ' \
                  'Exception from WebMock')
          begin
            client.send(:access_token)
          # rubocop:disable HandleExceptions
          rescue Faraday::Error::ConnectionFailed
          # rubocop:enable HandleExceptions
            # Nothing here becouse we just want to check requests count
          end
        end
      end
    end
  end

  describe 'configured?' do
    conf_keys = %w(rt dk id).map(&:to_sym)
    conf_keys.each do |config_key|
      it "return false when '#{config_key}' entry is missing" do
        override_config_key(config_key)
        expect(client.send(:configured?)).to be_false
      end
    end

    it 'return true when all required keys are set' do
      expect(client.send(:configured?)).to be_true
    end
  end

  describe 'get' do
    it 'respond with proper value' do
      expect(client.get('key_1')).to eq('Value 1')
    end

    it 'accept symbols' do
      expect(client.get(:key_1)).to eq('Value 1')
    end

    it "don't trigger API request when key is in keys store" do
      2.times { client.get('key_1') }
      a_request(:get, request_url).should have_been_made.once
    end

    it 'triggers API request when key is not in keys store' do
      client.get('key_1')
      a_request(:get, request_url).should have_been_made.once
    end

    it "don't trigger API request when key is in keys store" do
      2.times.each { client.get('key_1') }
      a_request(:get, request_url).should have_been_made.once
    end

    it "triggers API request when 'force_reload' is true" do
      2.times.each { client.get('key_1', true) }
      a_request(:get, request_url).should have_been_made.times(2)
    end

    it "triggers API request when 'volatile' is set in config" do
      override_config_key(:volatile, true)
      2.times.each { client.get('key_1') }
      a_request(:get, request_url).should have_been_made.times(2)
    end

    it "triggers request to 'https://api.keystok.com' when api_host is nil" do
      override_config_key(:api_host)
      client.get('key_1')
      a_request(:get, request_url(API_HOST))
        .should have_been_made.once
    end
  end

  describe 'keys' do
    before do
      stub_request(:get, request_url)
        .to_return(status: 200, body: example_response, headers: {})
    end

    it 'respond with proper values' do
      expect(client.keys).to eq('key_1' => 'Value 1', 'key_2' => 'Value 2')
    end

    it 'triggers API request when keys store is empty' do
      client.keys
      a_request(:get, request_url).should have_been_made.once
    end

    it "don't trigger API request when keys store is not empty" do
      2.times { client.keys }
      a_request(:get, request_url).should have_been_made.once
    end

    it "don't trigger API request when keys store is not empty" do
      2.times.each { client.keys }
      a_request(:get, request_url).should have_been_made.once
    end

    it "triggers API request when 'force_reload' is true" do
      2.times.each { client.keys(true) }
      a_request(:get, request_url).should have_been_made.times(2)
    end

    it "triggers API request when 'volatile' is set in config" do
      override_config_key(:volatile, true)
      2.times.each { client.keys(true) }
      a_request(:get, request_url).should have_been_made.times(2)
    end
  end

  describe 'check_config' do
    conf_keys = %w(rt dk id).map(&:to_sym)
    conf_keys.each do |config_key|
      it "raise ConfigError when '#{config_key}' entry is missing" do
        override_config_key(config_key)
        expect do
          client.send(:check_config)
        end.to raise_error(Keystok::Error::ConfigError,
                           "Config key: #{config_key} is missing!")
      end
    end
  end

  describe 'connection' do
    it 'initialize Faraday instance' do
      connection = client.send(:connection)
      expect(connection.url_prefix.to_s).to eq("#{config[:api_host]}/")
    end

    it "is using 'https://api.keystok.com' when api_host is nil" do
      override_config_key(:api_host)
      connection = client.send(:connection)
      expect(connection.url_prefix.to_s).to eq('https://api.keystok.com/')
    end

    it 'SSL verification is enabled' do
      connection = client.send(:connection)
      expect(connection.ssl.verify?).to eq(true)
    end

    it 'caches the instance' do
      connection_1 = client.send(:connection)
      connection_2 = client.send(:connection)
      expect(connection_2).to equal(connection_1)
    end
  end

  context 'config parsing' do
    let(:config_hash) { { api_host: 'http://dum.my', some_data: 2 } }
    describe 'decode_config' do
      it "returns input if it's a hash" do
        expect(client.send(:decode_config, config_hash)).to eq(config_hash)
      end

      it 'decodes access token and merges it to the config hash' do
        config_with_access_token = {
          access_token: Base64.urlsafe_encode64(config_hash.to_json)
        }
        expect(client.send(:decode_config,
                           config_with_access_token)).to eq(config_hash)
      end

      it "decodes access token when legacy 'connection_string' is used" do
        config_with_access_token = {
          connection_string: Base64.urlsafe_encode64(config_hash.to_json)
        }
        expect(client.send(:decode_config,
                           config_with_access_token)).to eq(config_hash)
      end

      it "raise ConfigError when access token can't be decoded" do
        config_with_access_token = {
          access_token: Base64.urlsafe_encode64(config_hash.to_json)
        }
        expect(client.send(:decode_config,
                           config_with_access_token)).to eq(config_hash)
      end

      it "returns hash if it's a JSON string" do
        expect(client.send(:decode_config,
                           config_hash.to_json)).to eq(config_hash)
      end

      it "returns hash if it's a base64 encoded JSON string" do
        encoded_hash = Base64.urlsafe_encode64(config_hash.to_json)
        expect(client.send(:decode_config,
                           encoded_hash.to_json)).to eq(config_hash)
      end

      it 'raise ConfigError exception on unknown input type' do
        expect do
          client.send(:decode_config, [:some, :array])
        end.to raise_error(Keystok::Error::ConfigError,
                           'Unknown config format')
      end
    end

    describe 'decode_json_string_config' do
      it 'respond with hash on JSON string' do
        expect(client.send(:decode_json_string_config,
                           config_hash.to_json)).to eq(config_hash)
      end

      it 'responds with nil when parsing JSON is failing' do
        expect(client.send(:decode_json_string_config,
                           'not_valid_json')).to eq(nil)
      end
    end

    describe 'decode_string_config' do
      it "returns hash if it's a JSON string" do
        expect(client.send(:decode_string_config,
                           config_hash.to_json)).to eq(config_hash)
      end

      it "returns hash if it's a base64 encoded JSON string" do
        encoded_hash = Base64.urlsafe_encode64(config_hash.to_json)
        expect(client.send(:decode_string_config,
                           encoded_hash.to_json)).to eq(config_hash)
      end

      it 'raise ConfigError exception on unknown input type' do
        expect do
          client.send(:decode_string_config, 'not_valid_json')
        end.to raise_error(Keystok::Error::ConfigError,
                           'Unknown config format')
      end

      it 'accepts empty string' do
        expect(client.send(:decode_string_config, '')).to eq({})
      end
    end
  end

  describe 'fetch_data' do
    context 'connection success' do
      before do
        stub_request(:get, request_url)
          .to_return(status: 200, body: example_response, headers: {})
      end

      it 'is making request to proper url' do
        client.send(:fetch_data)
        a_request(:get, request_url).should have_been_made.once
      end

      context 'is making request to proper url when key is not nil' do
        it 'and @config[:eager_fetching] is true' do
          client.send(:fetch_data, :some_key)
          a_request(:get, request_url).should have_been_made.once
        end

        it 'and @config[:eager_fetching] is false' do
          override_config_key(:eager_fetching, false)
          stub_deployment_req(config[:api_host], request_path(:some_key))
          client.send(:fetch_data, :some_key)
          a_request(:get, request_url(config[:api_host],
                                      request_path(:some_key)))
            .should have_been_made.once
        end
      end

      it 'is returning response data' do
        expect(client.send(:fetch_data).body).to eq(example_response)
      end
    end

    context 'connection fail' do
      context 'non 200 response status' do
        before do
          stub_request(:get, request_url)
            .to_return(status: 400, body: 'fail', headers: {})
        end

        it 'tries REQUEST_TRY_COUNT times before giving-up' do
          client.send(:fetch_data)
          a_request(:get, request_url).should have_been_made
            .times(Keystok::Client::REQUEST_TRY_COUNT)
        end

        it 'is returning non-200 response after 3 attempts' do
          expect(client.send(:fetch_data).status).to eq(400)
        end

        it 'should log warning on every non-200 response' do
          logger = double(:logger)
          Keystok.logger = logger
          expect(logger).to receive(:warn).exactly(3).times
            .with('Keystok API response status: 400')
          client.send(:fetch_data)
        end
      end

      context 'Faraday exception' do
        it 'tries REQUEST_TRY_COUNT times before giving-up' do
          stub_request(:get, request_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          begin
            client.send(:fetch_data)
          # rubocop:disable HandleExceptions
          rescue Faraday::Error::ConnectionFailed
          # rubocop:enable HandleExceptions
            # Nothing here becouse we just want to check requests count
          end
          a_request(:get, request_url).should have_been_made
            .times(Keystok::Client::REQUEST_TRY_COUNT)
        end

        it 'is re-raising Faraday exception after 3 attempts' do
          stub_request(:get, request_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          expect do
            client.send(:fetch_data)
          end.to raise_error(Faraday::Error::ConnectionFailed)
        end

        it 'should log warning on every Faraday exception' do
          stub_request(:get, request_url)
            .to_raise(Faraday::Error::ConnectionFailed)
          logger = double(:logger)
          Keystok.logger = logger
          expect(logger).to receive(:warn).exactly(3).times
            .with('Exception during Keystok API data fetch: ' \
                  'Exception from WebMock')
          begin
            client.send(:fetch_data)
          # rubocop:disable HandleExceptions
          rescue Faraday::Error::ConnectionFailed
          # rubocop:enable HandleExceptions
            # Nothing here becouse we just want to check requests count
          end
        end
      end
    end
  end

  describe 'keys_to_symbols' do
    it 'convert string hash keys to symbols' do
      expect(client.send(:keys_to_symbols, 'key_1' => 1, 'key_2' => 2))
        .to eq(key_1: 1, key_2: 2)
    end

    it 'leave non-string hash keys untouched' do
      expect(client.send(:keys_to_symbols, :key_1 => 1, 2 => 2, [3] => 3))
        .to eq(key_1: 1, 2 => 2, [3] => 3)
    end
  end

  describe 'load_keys' do
    before do
      FileUtils.rm_f(tmp_filepath)
    end

    after do
      FileUtils.rm_f(tmp_filepath)
    end

    context 'connection success' do
      before do
        stub_request(:get, request_url)
          .to_return(status: 200, body: example_response, headers: {})
      end

      it 'is triggering @keys_store setting' do
        client.send(:load_keys)
        expect(client.instance_variable_get('@keys_store'))
          .to eq('key_1' => 'Value 1', 'key_2' => 'Value 2')
      end

      it 'is returning loaded keys' do
        expect(client.send(:load_keys)).to eq('key_1' => 'Value 1',
                                              'key_2' => 'Value 2')
      end

      it 'is writing data to cache' do
        expect(File.size?(tmp_filepath).to_i).to eq(0)
        client.send(:load_keys)
        expect(File.size?(tmp_filepath).to_i).to_not eq(0)
      end

      it 'is not writing data to cache when no_cache is set in config' do
        override_config_key(:no_cache, true)
        expect(File.size?(tmp_filepath).to_i).to eq(0)
        client.send(:load_keys)
        expect(File.size?(tmp_filepath).to_i).to eq(0)
      end

      it 'is discarding old keys' do
        client.instance_variable_set('@keys_store', 'old' => 'value')
        expect(client.send(:load_keys)).to eq('key_1' => 'Value 1',
                                              'key_2' => 'Value 2')
      end

      it "when called with key name it passes key to 'fetch' method" do
        stub_deployment_req(config[:api_host], request_path(:some_key))
        override_config_key(:eager_fetching, false)
        expect(client).to receive(:fetch_data).with(:some_key)
          .and_call_original
        client.send(:load_keys, :some_key)
      end
    end

    context 'connection fail' do
      it 'is raising ConnectionError on no cache and response != 200' do
        stub_request(:get, request_url)
          .to_return(status: 400, body: 'fail', headers: {})
        expect do
          client.send(:load_keys)
        end.to raise_error(Keystok::Error::ConnectionError,
                           "No cache data and response code:\n" \
                             '400 with body: fail')
      end

      it 'is raising ConnectionError on no cache and Faraday exception' do
        stub_request(:get, request_url)
          .to_raise(Faraday::Error::ConnectionFailed)
        expect do
          client.send(:load_keys)
        end.to raise_error(Keystok::Error::ConnectionError,
                           /No cache data and connection error:.*/)
      end
    end

    context 'load from cache' do
      it 'loads cached data on response != 200' do
        stub_request(:get, request_url)
          .to_return(status: 200, body: example_response, headers: {})
        client.send(:load_keys)
        stub_request(:get, request_url)
          .to_return(status: 400, body: 'fail', headers: {})
        expect(client.send(:load_keys)).to eq('key_1' => 'Value 1',
                                              'key_2' => 'Value 2')
      end

      it 'loads cached data on Faraday exception' do
        stub_request(:get, request_url)
          .to_return(status: 200, body: example_response, headers: {})
        client.send(:load_keys)
        stub_request(:get, request_url)
          .to_raise(Faraday::Error::ConnectionFailed)
        expect(client.send(:load_keys)).to eq('key_1' => 'Value 1',
                                              'key_2' => 'Value 2')
      end
    end
  end

  describe 'oauth_client' do
    it 'initialize oauth client' do
      oauth_client = client.send(:oauth_client)
      expect(oauth_client.id).to eq(nil)
      expect(oauth_client.secret).to eq(nil)
      expect(oauth_client.site).to eq(config[:auth_host])
    end

    it "is using 'https://keystok.com' when auth_host is nil" do
      override_config_key(:auth_host)
      oauth_client = client.send(:oauth_client)
      expect(oauth_client.site).to eq('https://keystok.com')
    end

    it 'caches the instance' do
      oauth_client_1 = client.send(:oauth_client)
      oauth_client_2 = client.send(:oauth_client)
      expect(oauth_client_2).to equal(oauth_client_1)
    end
  end
end
