# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'
require 'hashie'

describe Keystok::CLI do
  let(:cli) { Keystok::CLI.new }
  let(:config) { { id: 1 } }
  let(:some_string) { Faker::Lorem.characters(20) }

  def access_token(id = '00')
    YAML.load_file(access_token_filepath(id))['access_token']
  end

  def access_token_filepath(id = '00')
    File.expand_path("../../fixtures/access_token_#{id}.data", __FILE__)
  end

  def suppress_puts
    allow($stdout).to receive(:puts)
  end

  before do
    suppress_puts
  end

  describe 'run' do
    before do
      stub_deployment_req(API_HOST, request_path)
      stub_request(:post, KEYSTOK_OAUTH_REFRESH_URL)
        .to_return(status: 200, body: refresh_response,
                   headers: { 'Content-Type' => 'application/json' })
    end

    it 'use ARGV by default' do
      begin
        original_argv = ARGV
        ARGV.clear
        ARGV << '-czzaaqq'
        expect { cli.run }.to raise_error(Keystok::Error::ConfigError)
      ensure
        ARGV.clear
        original_argv.each do |arg|
          ARGV << arg
        end
      end
    end

    it 'loads access token from file when filepath is provided' do
      expect_any_instance_of(Keystok::Client).to receive(:decode_string_config)
        .with(access_token).and_call_original
      cli.run(["-f#{access_token_filepath}"])
    end

    it "loads access token from legacy file with 'connection_string'" do
      expect_any_instance_of(Keystok::Client).to receive(:decode_string_config)
        .with(access_token).and_call_original
      cli.run(["-f#{access_token_filepath('02')}"])
    end

    it 'sets access token if provided' do
      expect_any_instance_of(Keystok::Client).to receive(:decode_string_config)
        .with(access_token).and_call_original
      cli.run(["-c#{access_token}"])
    end

    it 'use access token if both with filepath are provided' do
      expect_any_instance_of(Keystok::Client).to receive(:decode_string_config)
        .with(access_token).and_call_original
      cli.run(["-f#{access_token_filepath('01')}",
               "-c#{access_token}"])
    end

    it 'reports error when no -f or -c options are used' do
      expect($stdout).to receive(:puts).with('You have to use -c or -f option')
      expect { cli.run(['-f']) }.to raise_error { |error|
        expect(error).to be_a(SystemExit)
        expect(error.status).to eq(1)
      }
    end

    it "execute 'dump_data' when command is 'dump'" do
      expect(cli).to receive(:dump_data).and_call_original
      cli.run(["-c#{access_token}", '-adump'])
    end

    it "execute 'get' on client when command is 'get' with '-k'" do
      expect_any_instance_of(Keystok::Client).to receive(:get)
        .and_call_original
      cli.run(["-c#{access_token}", '-aget', '-kmy_key'])
    end

    it "execute 'format_keys' when command is 'get' without key_id" do
      expect_any_instance_of(Keystok::Client).to receive(:keys)
        .and_call_original
      expect(cli).to receive(:format_keys_list)
      cli.run(["-c#{access_token}", '-aget'])
    end

    it "execute 'format_keys_list' when command is unknown" do
      expect($stdout).to receive(:puts).with('Unknown command: ')
      expect do
        cli.run(["-c#{access_token}", '-asomething_else'])
      end.to raise_error { |error|
        expect(error).to be_a(SystemExit)
        expect(error.status).to eq(1)
      }
    end

    context 'launch init_token_file' do
      let(:tmp_access_token_filepath) do
        access_token_file = Tempfile.new('keystok_tests')
        FileUtils.cp(access_token_filepath, access_token_file.path)
        access_token_file.path
      end

      it "when 'init_token_file', 'access_token' is supplied" do
        expect(cli).to receive(:init_token_file).and_call_original
        cli.run(["-f#{tmp_access_token_filepath}", "-c#{access_token}", '-i'])
      end

      it "don't run when 'init_token_file' is not set" do
        expect(cli).to_not receive(:init_token_file)
        cli.run(["-f#{tmp_access_token_filepath}", "-c#{access_token}"])
      end

      it "don't run when 'access_token' is not supplied" do
        expect(cli).to_not receive(:init_token_file)
        cli.run(["-f#{tmp_access_token_filepath}", '-i'])
      end
    end
  end

  describe 'choose_access_token' do
    it "return access token passed as 'options.access_token'" do
      options = Hashie::Mash.new(access_token: 'dummy_token')
      expect(cli.send(:choose_access_token, options)).to eq('dummy_token')
    end

    it "return access token passed as 'options.access_token_filepath'" do
      options = Hashie::Mash.new(access_token_filepath: access_token_filepath)
      config_hash = YAML.load_file(access_token_filepath)
      expect(cli.send(:choose_access_token, options)).to eq(config_hash)
    end

    it "preffer 'options.access_token' over 'options.access_token_filepath'" do
      options = Hashie::Mash.new(access_token: 'dummy_token',
                                 access_token_filepath: access_token_filepath)
      expect(cli.send(:choose_access_token, options)).to eq('dummy_token')
    end
  end

  describe 'dump_data' do
    it 'defaults to :csv' do
      expect(cli).to receive(:dump_data_csv).with({})
      cli.send(:dump_data, {})
    end

    it 'exits 1 on unknown format' do
      expect($stdout).to receive(:puts).with('Unknown dump format: yaml2')
      expect { cli.send(:dump_data, {}, :yaml2) }.to raise_error { |error|
        expect(error).to be_a(SystemExit)
        expect(error.status).to eq(1)
      }
    end

    %w(csv json yaml).each do |format|
      it "execute 'dump_data_#{format}' on '#{format}' format" do
        expect(cli).to receive("dump_data_#{format}")
        cli.send(:dump_data, {}, format)
      end
    end
  end

  context do
    let(:keys) do
      hash = {}
      4.times do
        hash[Faker::Lorem.characters(rand(8) + 8)] = Faker::Lorem.sentence
      end
      hash
    end

    describe 'dump_data_csv' do
      it 'produces valid csv output' do
        output = cli.send(:dump_data_csv, keys)
        hash = {}
        CSV.parse(output, headers: true) do |row|
          hash[row['key_id']] = row['content']
        end
        expect(hash).to eq(keys)
      end
    end

    describe 'dump_data_json' do
      it 'produces valid json output' do
        output = cli.send(:dump_data_json, keys)
        hash = JSON.parse(output)
        expect(hash).to eq(keys)
      end
    end

    describe 'dump_data_yaml' do
      it 'produces valid yaml output' do
        output = cli.send(:dump_data_yaml, keys)
        hash = YAML.load(output)
        expect(hash).to eq(keys)
      end
    end
  end

  describe 'format_keys_list' do
    it 'return key names in separate lines' do
      keys = { a_key: 10, b_key: 20 }
      output = cli.send(:format_keys_list, keys)
      expect(output).to eq(keys.keys.join("\n"))
    end

    it 'return key names in alphabetical order' do
      keys = { b_key: 20, a_key: 10 }
      output = cli.send(:format_keys_list, keys)
      expect(output).to eq(keys.keys.sort.join("\n"))
    end
  end

  describe 'init_token_file' do
    let(:tmp_filepath) { Tempfile.new('keystok_tests') }

    it 'writes config file' do
      cli.send(:init_token_file, some_string, "#{tmp_filepath.path}_new")
      loaded_file = YAML.load_file("#{tmp_filepath.path}_new")
      expect(loaded_file[:access_token]).to eq(some_string)
    end

    it 'writes config file even when file is empty' do
      cli.send(:init_token_file, some_string, tmp_filepath.path)
      loaded_file = YAML.load_file(tmp_filepath.path)
      expect(loaded_file[:access_token]).to eq(some_string)
    end

    it 'updates existing config file' do
      tmp_filepath.write({ access_token: 'dummy' }.to_yaml)
      tmp_filepath.rewind
      cli.send(:init_token_file, some_string, tmp_filepath.path)
      loaded_file = YAML.load_file(tmp_filepath.path)
      expect(loaded_file[:access_token]).to eq(some_string)
    end

    it "don't change other keys in updated config file" do
      tmp_filepath.write({ my_another_key: 'dummy' }.to_yaml)
      tmp_filepath.rewind
      cli.send(:init_token_file, some_string, tmp_filepath.path)
      loaded_file = YAML.load_file(tmp_filepath.path)
      expect(loaded_file[:access_token]).to eq(some_string)
      expect(loaded_file[:my_another_key]).to eq('dummy')
    end
  end

  describe 'parse' do
    context 'default values' do
      [['command', :get],
       ['dump_format', :csv],
       ['access_token_filepath', File.expand_path('~/.keystok.yml')]
       ].each do |property, value|
        it "for '#{property}' is '#{value.inspect}'" do
          expect(cli.send(:parse, []).send(property)).to eq(value)
        end
      end
    end

    it 'works without argument' do
      expect { cli.send(:parse) }.to_not raise_error
    end

    context '--action' do
      it "set 'command'" do
        expect(cli.send(:parse, %w(--action get)).command).to eq(:get)
      end

      it "recognize '-a'" do
        expect(cli.send(:parse, ['-aget']).command).to eq(:get)
      end

      %w(dump get keys).map(&:to_sym).each do |action|
        it "accept '#{action}' command" do
          expect(cli.send(:parse, ["-a#{action}"]).command).to eq(action)
        end

      end
      it "don't allow custom actions" do
        expect(cli.send(:parse, ['-aget2']).command).to be_nil
      end
    end

    [%w(access-token c access_token),
     %w(access-token-file f access_token_filepath),
     %w(key-id k key_id)].each do |full_name, short_name, property|
      context "--#{full_name}" do
        it "set '#{property}'" do
          expect(cli.send(:parse, ["--#{full_name}", some_string])
            .send(property)).to eq(some_string)
        end

        it "recognize '-#{short_name}'" do
          expect(cli.send(:parse, ["-#{short_name}#{some_string}"])
            .send(property)).to eq(some_string)
        end
      end
    end

    context '--init-token-file' do
      it "set 'init_token_file'" do
        expect(cli.send(:parse, %w(--init-token-file)).init_token_file)
          .to eq(true)
      end

      it "recognize '-i'" do
        expect(cli.send(:parse, ['-i']).init_token_file).to eq(true)
      end
    end

    context '--dump-format-type' do
      it "set 'dump_format'" do
        expect(cli.send(:parse, %w(--dump-format-type yaml)).dump_format)
          .to eq(:yaml)
      end

      it "recognize '-t'" do
        expect(cli.send(:parse, ['-tyaml']).dump_format).to eq(:yaml)
      end

      %w(csv json yaml).map(&:to_sym).each do |format|
        it "accept '#{format}' format" do
          expect(cli.send(:parse, ["-t#{format}"]).dump_format).to eq(format)
        end

      end
      it "don't allow custom formats" do
        expect(cli.send(:parse, ['-tyaml2']).dump_format).to be_nil
      end
    end

    it "exit clean on '-h'" do
      expect { cli.send(:parse, ['-h']) }.to raise_error { |error|
        expect(error).to be_a(SystemExit)
        expect(error.status).to eq(0)
      }
    end
  end
end
