# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'
require 'stringio'
require 'tempfile'

describe Keystok::Cache do
  let(:dummy_class) do
    Class.new do
      attr_accessor :config
      include Keystok::AESCrypto
      include Keystok::Cache
    end
  end
  let(:dummy) { dummy_class.new }
  let(:iostream) { StringIO.new }
  let(:test_data) { 'data_' * 10 }
  let(:tmp_dir) { File.expand_path('../../../tmp', __FILE__) }
  let(:tmp_filepath) { File.join(tmp_dir, 'keystok_cache.data') }

  before do
    Keystok.logger = Logger.new(nil)
  end

  describe 'cache_file_path' do
    it 'works with dir passed as an argument' do
      path = dummy.cache_file_path('/some_dir')
      expect(path).to eq('/some_dir/keystok_cache.data')
    end

    it 'works without dir argument or config' do
      path = dummy.cache_file_path
      expect(path).to eq('./keystok_cache.data')
    end

    it 'works with dir in @config' do
      dummy.config = { tmp_dir: '/another_dir' }
      path = dummy.cache_file_path
      expect(path).to eq('/another_dir/keystok_cache.data')
    end
  end

  describe 'cache_file_exist?' do
    before do
      dummy.config = { tmp_dir: tmp_dir }
    end

    it 'returns true if file exists' do
      FileUtils.touch(tmp_filepath)
      expect(dummy.cache_file_exist?).to eq(true)
    end

    it 'returns false if file not exists' do
      FileUtils.rm_f(tmp_filepath)
      expect(dummy.cache_file_exist?).to eq(false)
    end
  end

  describe 'cache_stream' do
    before do
      dummy.config = { tmp_dir: tmp_dir }
      FileUtils.touch(tmp_filepath)
    end

    after do
      FileUtils.rm_f(tmp_filepath)
    end

    it 'is opening file for read when mode is :read' do
      expect { dummy.cache_stream(:read).read }.to_not raise_error
    end

    it 'is opening file for write when mode is :write' do
      expect { dummy.cache_stream(:write).write('dummy') }.to_not raise_error
    end

    it 'default mode is :read' do
      expect { dummy.cache_stream.read }.to_not raise_error
    end
  end

  describe 'load_cache' do
    before do
      iostream.write('some dummy data')
      iostream.seek(0)
    end

    it "logs warning when it's loading data" do
      logger = double(:logger)
      Keystok.logger = logger
      expect(logger).to receive(:warn).once
        .with('Loading data from cache')
      begin
        dummy.load_cache(iostream)
      # rubocop:disable HandleExceptions
      rescue Keystok::Error::CacheCorrupted
      # rubocop:enable HandleExceptions
        # Suppressing because we just want to check logger message
      end
    end
  end

  describe 'write_cache' do
    it 'works' do
      expect { dummy.write_cache(test_data, iostream) }.to_not raise_error
    end
  end

  describe 'sanity check' do
    it 'saving and loading is not changing data' do
      dummy.write_cache(test_data, iostream)
      iostream.seek(0)
      expect(dummy.load_cache(iostream)).to eq(test_data)
    end
  end
end
