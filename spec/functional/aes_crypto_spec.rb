# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'

describe Keystok::AESCrypto do
  let(:dummy_class) { Class.new { include Keystok::AESCrypto } }
  let(:dummy) { dummy_class.new }

  describe 'cipher' do
    it 'works without any errors' do
      dummy.cipher('plain text', 'key_' * 10, 'vector_' * 10, 256, :CBC)
    end

    it 'works without passing key_size and block_mode' do
      dummy.cipher('plain text', 'key_' * 10, 'vector_' * 10)
    end

    it 'is passing OpenSSL exceptions' do
      expect do
        dummy.cipher('plain text', 'a', 'vector_' * 10)
      end.to raise_error(OpenSSL::Cipher::CipherError)
    end
  end

  describe 'decipher' do
    let(:encrypted) { "A.\xFC\xDDW=\xB7\xCF\x02\x88\xE3:sA\x90S" }

    it 'works without any errors' do
      dummy.decipher(encrypted, 'key_' * 10, 'vector_' * 10, 256, :CBC)
    end

    it 'works without passing key_size and block_mode' do
      dummy.decipher(encrypted, 'key_' * 10, 'vector_' * 10)
    end

    it 'is passing OpenSSL exceptions' do
      expect do
        dummy.decipher('encrypted', 'a', 'vector_' * 10)
      end.to raise_error(OpenSSL::Cipher::CipherError, 'key length too short')
    end
  end

  describe 'sanity check' do
    it 'decryption of encrypted data returns proper value' do
      plain_text = 'plain text'
      encrypted = dummy.cipher(plain_text, 'key_' * 10, 'vector_' * 10)
      decrypted_text = dummy.decipher(encrypted, 'key_' * 10, 'vector_' * 10)
      expect(decrypted_text).to eq(plain_text)
    end
  end

  describe 'decrypt_key' do
    let(:encrypted_data) do
      File.open(File.expand_path('../../fixtures/encrypted_data_00.data',
                                 __FILE__)).read
    end
    let(:config) do
      { dk:
          '965391f2bba37b4586431b8690d7044d5cdc73adf7dda539f8dd3a60cb3e9b12' }
    end

    it 'properly decrypts data' do
      expect(dummy.decrypt_key(encrypted_data, config)).to eq('Value 1')
    end

    it "raise UnsupportedDataFormat when data prefix is no ':aes256'" do
      expect do
        dummy.decrypt_key(encrypted_data.sub(':aes256:', ':cesar:'), config)
      end.to raise_error(Keystok::Error::UnsupportedDataFormat,
                         'Wrong encryption algorithm')
    end

    it 'raise ConfigError when no decryption key is provided' do
      expect do
        dummy.decrypt_key(encrypted_data, {})
      end.to raise_error(Keystok::Error::ConfigError,
                         'No decryption key in config')
    end

    it 'is passing OpenSSL exceptions' do
      expect do
        dummy.decrypt_key(encrypted_data, dk: 'a')
      end.to raise_error(OpenSSL::Cipher::CipherError,
                         'bad decrypt')
    end
  end
end
