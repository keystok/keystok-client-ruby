# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'

describe Keystok do
  describe 'logger' do
    before do
      @rails = Rails
      Kernel.silence_warnings do
        # rubocop: disable ConstantName
        Rails = nil
        # rubocop: enable ConstantName
      end
      load 'keystok.rb'
    end

    after do
      Kernel.silence_warnings do
        # rubocop: disable ConstantName
        Rails = @rails
        # rubocop: enable ConstantName
      end
      load 'keystok.rb'
    end

    it 'create default logger' do
      expect(Keystok.logger.respond_to?(:warn)).to eq(true)
    end
  end
end
