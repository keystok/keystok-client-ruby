# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'spec_helper'
require 'fileutils'
require 'stringio'

require 'rails'
require 'rails/generators'
require 'generators/keystok/install_generator'

describe 'Rails integration' do
  before do
    load 'keystok.rb'
  end

  describe 'logger' do
    it 'is using Rails.logger when Rails is defined' do
      initializer = Keystok::Railtie.initializers.find do |initializer_tmp|
        initializer_tmp.name == 'keystok.configure_rails_initialization'
      end
      initializer.run
    end
  end

  describe 'generator' do
    let(:destination_dir) { File.expand_path('../../../tmp/tests', __FILE__) }

    before do
      FileUtils.rm_rf(destination_dir)
      FileUtils.mkdir_p(destination_dir)
      @std_out_original = $stdout
      $stdout = StringIO.new
    end

    after do
      $stdout = @std_out_original
      FileUtils.rm_rf(destination_dir)
    end

    it 'creates initializer' do
      Keystok::Generators::InstallGenerator.start(['access_token'],
                                                  destination_root:
                                                    destination_dir)
      expect(File.size(File.join(destination_dir,
                                 'config',
                                 'initializers',
                                 'keystok.rb'))).to be > 0
    end

    context 'config file' do
      let(:loaded_config) do
        YAML.load_file(File.join(destination_dir, 'config', 'keystok.yml'))
      end

      before do
        Keystok::Generators::InstallGenerator.start(['access_token'],
                                                    destination_root:
                                                      destination_dir)
      end

      it 'creates config file' do
        expect(File.size(File.join(destination_dir,
                                   'config',
                                   'keystok.yml'))).to be > 0
      end

      %w(production development test).map(&:to_sym).each do |env|
        it "config file have :access_token set in #{env} environment" do
          expect(loaded_config[env][:access_token])
            .to eq('access_token')
        end

        it "config file have :tmp_dir set in #{env} environment" do
          expect(loaded_config[env][:tmp_dir]) .to eq('tmp')
        end
      end
    end
  end
end
