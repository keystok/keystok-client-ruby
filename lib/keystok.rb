# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'csv'
require 'faraday'
require 'json'
require 'logger'
require 'yaml'

require 'keystok/version'
require 'keystok/errors'
require 'keystok/cli'
require 'keystok/client'

#:nodoc:
module Keystok
  class << self
    attr_accessor :logger
  end
end

if defined? Rails::Railtie
  require 'keystok/railtie'
else
  Keystok.logger = Logger.new(STDOUT)
end
