# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

module Keystok
  module Error
    class CacheCorrupted < StandardError
    end

    class ConfigError < StandardError
    end

    class ConnectionError < StandardError
    end

    class UnsupportedDataFormat < StandardError
    end
  end
end
