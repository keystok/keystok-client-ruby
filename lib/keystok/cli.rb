# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'optparse'
require 'ostruct'

module Keystok
  # Parses command line arguments and execute requested actions
  class CLI
    def run(args = ARGV)
      options = parse(args)
      access_token = choose_access_token(options)
      if access_token.nil?
        puts 'You have to use -c or -f option'
        exit 1
      end
      client = Keystok::Client.new(access_token)
      case options.command
      when :dump
        puts dump_data(client.keys, options.dump_format)
      when :get
        if options.key_id
          puts client.get(options.key_id)
        else
          puts format_keys_list(client.keys)
        end
      else
        puts "Unknown command: #{options.command}"
        exit 1
      end
      if options.init_token_file && options.access_token &&
         options.access_token_filepath
        init_token_file(access_token, options.access_token_filepath)
      end
    end

    private

    def choose_access_token(options)
      access_token = options.access_token if options.access_token
      if !access_token && options.access_token_filepath
        access_token = YAML.load_file(options.access_token_filepath)
      end
      access_token
    end

    def dump_data(keys, format = 'csv')
      unless %w(csv json yaml).include?(format.to_s)
        puts "Unknown dump format: #{format}"
        exit 1
      end
      send("dump_data_#{format}", keys)
    end

    def dump_data_csv(keys)
      CSV.generate do |csv|
        csv << %w(key_id content)
        keys.each do |key_id, content|
          csv << [key_id, content]
        end
      end
    end

    def dump_data_json(keys)
      keys.to_json
    end

    def dump_data_yaml(keys)
      keys.to_yaml
    end

    def format_keys_list(keys)
      keys.keys.sort.join("\n")
    end

    def init_token_file(access_token, filepath)
      config = {}
      config = YAML.load_file(filepath) || {} if File.exist?(filepath)
      config[:access_token] = access_token
      File.open(filepath, 'w') { |file| file.write(config.to_yaml) }
    end

    # rubocop:disable MethodLength
    def parse(args = [])
      options = OpenStruct.new
      options.command = :get
      options.access_token_filepath = File.expand_path('~/.keystok.yml')
      options.dump_format = :csv
      opts_parser = OptionParser.new do |opts|
        opts.banner = 'Usage: keystok_rb [options] COMMAND'
        opts.separator ''
        opts.separator 'Options:'
        opts.on('-a', '--action [ACTION]', [:dump, :get, :keys],
                'Action to perform (dump, get, keys)') do |ext|
          options.command = ext
        end
        opts.on('-c', '--access-token [access_token]',
                'Use access_token when contacting Keystok API') do |ext|
          options.access_token = ext
        end
        opts.on('-f', '--access-token-file [FILE_PATH]',
                'Use content of file at FILE_PATH as access token',
                ' when contacting Keystok API') do |ext|
          options.access_token_filepath = ext
        end
        opts.on('-i', '--init-token-file', 'Create/update access token file',
                'with value of \'-c\' option') do |_|
          options.init_token_file = true
        end
        opts.on('-k', '--key-id [KEY_ID]', 'When action is GET, fetch value',
                ' for this KEY_ID') do |ext|
          options.key_id = ext || ''
        end
        opts.on('-t', '--dump-format-type [TYPE]', [:csv, :json, :yaml],
                'Format of data dump file (csv, json, yaml)') do |ext|
          options.dump_format = ext
        end
        opts.on_tail('-h', '--help', 'Show this message') do
          puts opts
          exit
        end
      end
      opts_parser.parse!(args)
      options
    end
    # rubocop:enable MethodLength
  end
end
