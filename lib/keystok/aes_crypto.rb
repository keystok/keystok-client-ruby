# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

require 'base64'
require 'json'
require 'openssl'

module Keystok
  # Module handling data encryption and decryption
  # It also handles raw data from API unpacking
  module AESCrypto
    PREFIX = ':aes256:'

    def cipher(plain_text, key, iv, key_size = 256, block_mode = :CBC)
      cipher = OpenSSL::Cipher::AES.new(key_size, block_mode)
      cipher.encrypt
      cipher.key = key
      cipher.iv = iv
      cipher.update(plain_text) + cipher.final
    end

    def decipher(encrypted, key, iv, key_size = 256, block_mode = :CBC)
      decipher = OpenSSL::Cipher::AES.new(key_size, block_mode)
      decipher.decrypt
      decipher.key = key
      decipher.iv = iv
      decipher.update(encrypted) + decipher.final
    end

    def decrypt_key(encrypted_key, config = nil)
      config ||= @config || {}
      fail Error::ConfigError, 'No decryption key in config' unless config[:dk]
      unless encrypted_key.start_with?(PREFIX)
        fail Error::UnsupportedDataFormat, 'Wrong encryption algorithm'
      end
      encrypted_data = Base64.decode64(encrypted_key.sub(/^:[^:]+:/, ''))
      json_data = JSON.parse(encrypted_data)
      key = OpenSSL::PKCS5.pbkdf2_hmac_sha1(config[:dk],
                                            Base64.decode64(json_data['salt']),
                                            json_data['iter'],
                                            json_data['ks'] / 8)
      decipher(Base64.decode64(json_data['ct']), key,
               Base64.decode64(json_data['iv']), json_data['ks'])
    end
  end
end
