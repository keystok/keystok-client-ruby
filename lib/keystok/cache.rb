# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

module Keystok
  # Module for handling cache write and read operations
  module Cache
    def cache_file_path(tmp_dir = nil)
      tmp_dir ||= (@config && @config[:tmp_dir]) ? @config[:tmp_dir] : '.'
      @cache_file_path ||= File.join(tmp_dir, 'keystok_cache.data')
    end

    def cache_file_exist?
      File.exist?(cache_file_path)
    end

    def cache_stream(method = :read)
      case method
      when :write
        mode = 'wb'
      else
        mode = 'r'
      end
      File.open(cache_file_path, mode)
    end

    def load_cache(iostream = nil)
      Keystok.logger.warn('Loading data from cache')
      iostream ||= cache_stream
      iostream.read
    end

    def write_cache(cache_data, iostream = nil)
      iostream ||= cache_stream(:write)
      iostream.write(cache_data)
      iostream.flush
    end
  end
end
