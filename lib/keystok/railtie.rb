# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

module Keystok
  # Class used to integrate with Rails
  class Railtie < Rails::Railtie
    initializer 'keystok.configure_rails_initialization' do
      Keystok.logger = Rails.logger
    end
  end
end
