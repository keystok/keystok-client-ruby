config_filepath = File.expand_path('../../keystok.yml', __FILE__)
config_data = YAML.load_file(config_filepath)[Rails.env.to_sym]
if config_data.nil?
  fail "Config file error in #{Rails.env} (#{config_filepath})"
end
KEYSTOK = Keystok::Client.new(config_data)
