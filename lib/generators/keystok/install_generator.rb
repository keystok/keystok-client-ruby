# encoding: utf-8
# This file is distributed under GitDock Oy license terms.
# See https://bitbucket.org/keystok/keystok-client-ruby/raw/master/LICENSE.txt
# for details.

module Keystok
  module Generators
    # Generator that creates config file and initializer
    # It also add config file to .gitignore
    class InstallGenerator < Rails::Generators::Base
      argument :access_token,
               type: :string,
               banner: 'access_string'

      source_root File.expand_path('../templates', __FILE__)

      desc 'Create initializer'

      def create_initializer_and_config_file
        template 'keystok.yml.erb', 'config/keystok.yml'
        template 'keystok.rb',      'config/initializers/keystok.rb'
      end
    end
  end
end
