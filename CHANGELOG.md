# Change log

## master (unreleased)

## 1.3.0 (2014-05-12)

### Changes
* **--init-token-file** added to eliminate a need of creating manually ~/.keystok.yml
  Now it can be created along with executing normal keystok command

## 1.2.0 (2014-05-07)

### Changes
* **connection_string** renamed to less confusing **access_token**.
  Please note that **connection_string** is not deprecated in config files
  and it will be still suppoerted except CLI long option name.
  However it will not be mentioned in documentation anymore.

## 1.1.1 (2014-05-05)

### Bugfixes
* 1 - Fails on heroku

## 1.1.0 (2014-04-24)

### New features
* CLI client (keystok_rb)
