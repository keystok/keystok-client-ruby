# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'keystok/version'

Gem::Specification.new do |spec|
  spec.name          = 'keystok'
  spec.version       = Keystok::VERSION
  spec.authors       = ['Piotr Sokolowski']
  spec.email         = ['piotr@keystok.com']
  spec.summary       = %q{Keystok API client}
  spec.description   = <<-EOS
Ruby client for Keystok service. https://keystok.com
EOS
  spec.homepage      = 'https://keystok.com'
  spec.license       = 'GitDock Oy'
  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'faraday'
  spec.add_dependency 'oauth2'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'ffaker'
  spec.add_development_dependency 'fuubar'
  spec.add_development_dependency 'hashie'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rails', '>= 3.0'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'webmock'
end
